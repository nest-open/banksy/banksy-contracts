require("@nomicfoundation/hardhat-toolbox");
require("dotenv").config()
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.20",
  networks : {
    tob  : {
      url : "https://rpc-public-mainnet.volary.io/",
      accounts : [process.env.BANKSY_KEY]
    }
  }
};
