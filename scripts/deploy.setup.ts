
const hardhat = require("hardhat")
async function main() {
    const signers = (await hardhat.ethers.getSigners());
    const owner = signers[0];
    //const rewardsSigner = signers[1];
    console.log("Deploying contracts with the account:", owner.address);

    let token = await hardhat.ethers.getContractFactory("TestSuperERC20");
    token = await token.deploy("BanksyERC20","BKSY",hardhat.ethers.parseEther('100000000'));
    console.log("token address is -- ",token.target);

    let  ERC6551Account = await hardhat.ethers.getContractFactory("ERC6551Account");
    ERC6551Account = await ERC6551Account.deploy();

    console.log("ERC6551Account Address --- ",ERC6551Account.target);

    let  ERC6551Registry = await hardhat.ethers.getContractFactory("ERC6551Registry");
    ERC6551Registry = await ERC6551Registry.deploy();

    console.log("ERC6551Registry Address --- ",ERC6551Registry.target);

    let  banksyNFT = await hardhat.ethers.getContractFactory("banksyNFT");
    banksyNFT = await banksyNFT.deploy("banksyNFT","BKSY",token.target,ERC6551Registry.target,ERC6551Account.target);

    console.log("banksyNFT address -- ",banksyNFT.target)

  
    // let stacking = await hardhat.ethers.getContractFactory("banksyNFT");
    // stacking =await stacking.deploy(process.env.TOKEN_ADDRESS,process.env.TREASURY_ADDRESS);
    // let stakingAddress = stacking.address;
    // console.log("Staking address:", stakingAddress);
    // console.log("stacking deployed")

    // let rewards = await hardhat.ethers.getContractFactory("contracts/rewards.sol:rewardPool");
    // rewards = await rewards.connect(signers[1]).deploy(process.env.TOKEN_ADDRESS,stakingAddress,process.env.TREASURY_ADDRESS);
    // let rewardsAddress = rewards.address;
    // console.log("rewards address:", rewardsAddress);
    // console.log("rewards deployed")
    // const setRewardPool = await stacking.addRewardPoolAddress(rewardsAddress);
    // console.log(setRewardPool)

}

main();