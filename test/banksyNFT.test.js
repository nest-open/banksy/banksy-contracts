const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("banksyNFT Contract", function () {
  let token;
  let registry;
  let accountImplementaion;
  let BanksyNFT;
  let banksyNFT;
  let owner;
  let addr1;
  let addr2;
  let addrs;
  let ERC6551Registry;
  let ERC6551Account;

  beforeEach(async function () {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

   token = await ethers.getContractFactory("TestSuperERC20");
   token = await token.deploy("token","tkn",ethers.parseEther('1000000'));

   ERC6551Registry = await ethers.getContractFactory("ERC6551Registry");
   registry = await ERC6551Registry.deploy();

   ERC6551Account= await ethers.getContractFactory("ERC6551Account");
   accountImplementaion= await ERC6551Account.deploy();

    // Deploy banksyNFT contract
    BanksyNFT = await ethers.getContractFactory("banksyNFT");
    banksyNFT = await BanksyNFT.deploy("BanksyNFT", "BNFT", token.target,registry.target,accountImplementaion.target); // Replace with the actual account implementation address

  });

  it("e2e flow testing",async()=> {
    await token.transfer(banksyNFT.target,ethers.parseEther('10000'));
    let mintTx = await banksyNFT.connect(owner).mint(owner.address, "tokenURI1", 100, 1, "0x");
    mintTx = await mintTx.wait();
    const [event] = mintTx.logs.filter(log => log?.fragment?.name === "NFTMinted");
    expect(event.args[0].toLowerCase()).to.be.equal(owner.address.toLowerCase());
    expect(await banksyNFT.tokenURI(1)).to.be.equal("tokenURI1")
    const accountAddress = event.args[1];
    expect(ethers.isAddress(accountAddress)).to.be.equal(true);
    expect(Number(event.args[2])).to.be.equal(1);
    const addressFromRegistry = await registry.account(
        accountImplementaion.target,
        31337,
        banksyNFT.target,
        1,
        1
      );
    
    expect(addressFromRegistry.toLowerCase()).to.be.equal(accountAddress.toLowerCase());
    const myAccountContract = await hre.ethers.getContractAt("ERC6551Account", accountAddress);
    const ownerOfMyAccount = await myAccountContract.owner();
    expect(ownerOfMyAccount.toLowerCase()).to.be.equal(owner.address.toLowerCase());
    let tokenBalanceOfAccount = await token.balanceOf(accountAddress);
    expect(Number(tokenBalanceOfAccount)).to.be.equal(100);
    expect(Number(await banksyNFT.getBalanceOfNFT(1,1))).to.be.equal(100);
    await expect(banksyNFT.connect(addr1).burnNFT(1,1),"Only Token holder can burn")
    await banksyNFT.burnNFT(1,1);
    await expect(banksyNFT.ownerOf(1)).to.be.reverted;
    tokenBalanceOfAccount = await token.balanceOf(accountAddress);
    expect(Number(tokenBalanceOfAccount)).to.be.equal(0)
  })

  it("test ERC6551Account is initialise only once", async() => {
    await accountImplementaion.initialize(token.target);
    const admin =await  accountImplementaion.admin()
    expect(admin.toLowerCase()).to.be.equal(owner.address.toLowerCase());
    const stableCoinAddress = await accountImplementaion.stableCoinAddress();
    expect(stableCoinAddress.toLowerCase()).to.be.equal(token.target.toLowerCase());
    await expect(accountImplementaion.initialize(token.target)).to.be.rejectedWith("Contract initialised");
  })

});
