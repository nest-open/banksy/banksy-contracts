// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "hardhat/console.sol";

import {IERC6551Registry} from "./interfaces/IERC6551Registry.sol";
import {IERC6551Account} from "./interfaces/IERC6551Account.sol";

contract banksyNFT is ERC721URIStorage, Ownable, ERC721Burnable {
    uint256 _tokenIdCounter;
    IERC20 token;
    IERC6551Registry registry;
    address accountImplementation;
    event NFTMinted(address indexed,address indexed,uint256);

    constructor(string memory name, string memory symbol, address tokenAddress, address registryAddress,address _accountImplementation) ERC721(name, symbol) Ownable(msg.sender) {
        token = IERC20(tokenAddress);
        registry = IERC6551Registry(registryAddress); 
        accountImplementation = _accountImplementation;
    }


    function mint(address to, string memory uri,uint256 amount,uint256 seed,bytes calldata initData) public  onlyOwner{
        uint256 tokenId = ++_tokenIdCounter;
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
        address accountAddress = registry.createAccount(accountImplementation,block.chainid,address(this),tokenId,seed,initData);
        bool success = IERC6551Account(payable(accountAddress)).initialize(address(token));
        require(success,"Error Initalizing Account");
        bool transferSuccess = token.transfer(accountAddress,amount);
        require(transferSuccess,"Error transfering token to Account");
        emit NFTMinted(to, accountAddress, tokenId);
    }

    function burnNFT(uint256 _tokenId,uint256 salt) public returns(bool){
        require(msg.sender == ownerOf(_tokenId),"Only Token holder can burn");
        _burn(_tokenId);
        address accountAddress = registry.account(accountImplementation, block.chainid, address(this), _tokenId, salt);
        bool success = IERC6551Account(payable(accountAddress)).burnNft();
        return true;
    }


    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function getBalanceOfNFT(uint256 _tokenId,uint256 salt) public view returns(uint256){
       address accountAddress = registry.account(accountImplementation, block.chainid, address(this), _tokenId, salt);
       return token.balanceOf(accountAddress);
    }
}
