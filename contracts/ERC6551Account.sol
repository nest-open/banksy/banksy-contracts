// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.20;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol"; 
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol"; 
import "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./interfaces/IERC6551Account.sol";
import "./lib/ERC6551AccountLib.sol";

contract ERC6551Account is IERC165, IERC6551Account,ERC1155Holder{
    uint256 public nonce;
    address public admin;
    IERC20  stableCoin;

    function initialize(address tokenAddress) public returns(bool){
        require(admin == address(0) && address(stableCoin) == address(0),"Contract initialised");
        admin = msg.sender;
        stableCoin = IERC20(tokenAddress);
        return true;
    }

    receive() external payable {}

    function executeCall(
        address to,
        uint256 value,
        bytes calldata data
    ) internal  {
      // dummy implementaion
    }

    function token()
        external
        view
        returns (
            uint256,
            address,
            uint256
        )
    {
        return ERC6551AccountLib.token();
    }

    function owner() public view returns (address) {
        (uint256 chainId, address tokenContract, uint256 tokenId) = this.token();
        if (chainId != block.chainid) return address(0);

        return IERC721(tokenContract).ownerOf(tokenId);
    }

    function burnNft() public returns(bool){
        require(msg.sender == admin,"Only banksy NFT can burn");
        uint256 amount = stableCoin.balanceOf(address(this));
        bool transferSuccess= stableCoin.transfer(admin, amount);
        require(transferSuccess, "Token transfer failed");
        return true;
    }

    function stableCoinAddress() public view returns(address){
        return address(stableCoin);
    }

   
}